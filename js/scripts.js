jQuery(document).ready(function(){
  (function(){
    var count = 0;
    var piecon_color = Drupal.settings.piecon.color;
    var piecon_background = Drupal.settings.piecon.background;
    var piecon_shadow = Drupal.settings.piecon.shadow;
    var piecon_percent = Drupal.settings.piecon.percent;
    Piecon.setOptions({color: piecon_color, background: piecon_background, shadow: piecon_shadow, fallback: piecon_percent});
    var i = setInterval(function(){
      if (++count > 100) { Piecon.reset(); clearInterval(i); return false; }
      Piecon.setProgress(count);
    }, 250);
  })(jQuery);
  jQuery('#edit-piecon-color, #edit-piecon-backgroundcolor, #edit-piecon-shadow').ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
      jQuery(el).val('#' + hex);
      jQuery(el).ColorPickerHide();
    },
    onBeforeShow: function () {
      jQuery(this).ColorPickerSetColor(this.value);
    }
  });
});
  